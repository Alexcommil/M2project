﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    public Vector2 Look;
    public float rotationX;
    public float rotationY;
    public float lookSpeed = 5;


    // Start is called before the first frame update
    void Start()
    {
    
    }
    public void OnLook(InputAction.CallbackContext ctx)
    {

        Look = ctx.ReadValue<Vector2>();

    }

    // Update is called once per frame
    void Update()
    {
        rotationX += Look.y * lookSpeed;
        rotationY += Look.x * lookSpeed;
        rotationX = Mathf.Clamp(rotationX, -90, 90);
        transform.rotation = Quaternion.Euler(-rotationX, rotationY, 0);


        
        
    }
}
