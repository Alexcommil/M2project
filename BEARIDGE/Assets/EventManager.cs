﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventManager : MonoBehaviour
{
    public Event currentEvent;
    public string alt;
    public AudioSource audioSource;
    public bool playingEvent;
    public float eventDelay;
    //public float lookTimer;
    public bool waitingForAlt;
    public float lookDelay;
    //public Image lookUI;

    public Camera cam;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //cam.farClipPlane = Mathf.Lerp(cam.farClipPlane, 1000, Time.deltaTime);
       
        //Debug.Log(currentEvent.ID);
        if (!audioSource.isPlaying)
        {
            playingEvent = false;
            eventDelay -= Time.deltaTime;
            if(eventDelay < 0 && !currentEvent.isEnd && !waitingForAlt)
            {
                PlayEvent();
            }

        if(waitingForAlt && alt !="")
            {
                foreach (Event item in currentEvent.next)
                {
                    if (item.altName == alt)
                    {
                        currentEvent = item;
                        eventDelay = currentEvent.eventDelay;
                        waitingForAlt = false;
                        //alt = "";
                        break;
                    }
                }

            }


        }

        //lookUI.fillAmount = Mathf.Clamp01(lookTimer / lookDelay);

    }

    public void PlayEvent()
    {
        playingEvent = true;
        
        audioSource.clip = currentEvent.audio;
        audioSource.Play();
        eventDelay = currentEvent.eventDelay;
        if(!currentEvent.hasAlts)
        {
        currentEvent = currentEvent.next[0];
            //alt = "";

        }
        else
        {
            if(!currentEvent.requiresAlt)
            foreach (Event item in currentEvent.next)
            {
                if(item.altName == alt || item.altName == "")
                {
                    currentEvent = item;
                    //alt = "";
                    break;
                }
            }
            else
            {
                waitingForAlt = true;
            }
        }

    }
}
