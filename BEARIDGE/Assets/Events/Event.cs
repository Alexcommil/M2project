﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Event", menuName = "ScriptableObjects/Events", order = 1)]
public class Event : ScriptableObject
{
    public int ID;
    public string eventName;
    public bool hasAlts;
    public bool requiresAlt;
    public string altName;
    public AudioClip audio;
    public float eventDelay;
    public AnimationClip animClip;
    public List<Event> next;

    public bool isEnd;



}
