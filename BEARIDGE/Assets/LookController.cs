﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookController : MonoBehaviour
{
    public EventManager eventManager;
    public GameObject currentLookedAt;
    public RaycastHit hit;
    
    public float angleX;
    public float angleY;
    public float deltaX = 0;
    public float deltaY = 0;
    public int yesCounter;
    public int yesNegCounter;
    public int noCounter;
    public int noNegCounter;
    public float nodAngleTreshold = 30;
    public float nodCountTreshold = 4;

    private float nodDecayTimer = 1;
    // Start is called before the first frame update
    void Start()
    {
        //cam = GetComponent<Camera>();
        angleX = transform.rotation.eulerAngles.x;
        angleY = transform.rotation.eulerAngles.y;

    }

    // Update is called once per frame
    void Update()
    {
        deltaX += angleX - transform.rotation.eulerAngles.x;
        deltaY += angleY - transform.rotation.eulerAngles.y;
        angleX = transform.rotation.eulerAngles.x;
        angleY = transform.rotation.eulerAngles.y;

        if (deltaX > 355 || deltaX < -355)
        {
            deltaX = 0;
        }
        if (deltaY > 355 | deltaY < -355)
        {
            deltaY = 0;
        }





        if (deltaX > 0)
        {
            deltaX -= Time.deltaTime * 30;
        }
        if (deltaY > 0)
        {
            deltaY -= Time.deltaTime * 30;
        }


        if (deltaX > nodAngleTreshold)
        {
            deltaX = 0;
            deltaY = 0;
            if (yesCounter <= nodCountTreshold + 1)
            {
                yesCounter++;

            }
            noCounter = 0;
            noNegCounter = 0;


        }
        if (deltaY > nodAngleTreshold)
        {
            deltaX = 0;
            deltaY = 0;
            if (noCounter <= nodCountTreshold + 1)
            {
                noCounter++;

            }
            yesCounter = 0;
            yesNegCounter = 0;


        }
        if (deltaX < -nodAngleTreshold)
        {
            deltaX = 0;
            deltaY = 0;
            if (yesNegCounter <= nodCountTreshold + 1)
            {
                yesNegCounter++;

            }
            noCounter = 0;
            noNegCounter = 0;

        }
        if (deltaY < -nodAngleTreshold)
        {
            deltaX = 0;
            deltaY = 0;
            if(noNegCounter <= nodCountTreshold + 1)
            {
            noNegCounter++;

            }
            yesCounter = 0;
            yesNegCounter = 0;


        }

        nodDecayTimer -= Time.deltaTime;
        if(nodDecayTimer < 0)
        {
            if(yesCounter != 0)
            {
                yesCounter--;
                nodDecayTimer = 1;
            }if(yesNegCounter != 0)
            {
                
                yesNegCounter--;
                nodDecayTimer = 1;
            }
            if (noCounter != 0)
            {
                noCounter--;
                nodDecayTimer = 1;

            }if (noNegCounter != 0)
            {
                
                noNegCounter--;
                nodDecayTimer = 1;

            }
        }

        if(yesCounter >= nodCountTreshold && yesNegCounter >= nodCountTreshold)
        {
            yesCounter = 0;
            yesNegCounter = 0;
            eventManager.alt = "yes";
            Debug.Log("said yes");

        }
        if (noCounter >= nodCountTreshold && noNegCounter >= nodCountTreshold)
        {
            noCounter = 0;
            noNegCounter = 0;
            eventManager.alt = "no";
            Debug.Log("said no");

        }


        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            if (hit.collider.gameObject != currentLookedAt)
            {
                if (hit.collider.gameObject.CompareTag("POI"))
                {
                    if (currentLookedAt != null)
                    {
                        currentLookedAt.GetComponent<POIController>().OnStopLooking();

                    }
                    currentLookedAt = hit.collider.gameObject;
                    currentLookedAt.GetComponent<POIController>().OnStartLooking();
                }
                else
                {
                    if (currentLookedAt != null)
                    {
                        currentLookedAt.GetComponent<POIController>().OnStopLooking();

                    }
                    currentLookedAt = null;
                }


            }
            else
            {

            }


        }
    }
}
