﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POIController : MonoBehaviour
{
    // Start is called before the first frame update

    public EventManager eventManager;
    public string alt;
    bool beingLookedAt = false;
    public float lookTimer;
    public MeshRenderer meshRenderer;
    public Color defaultMatColor;
    public Color lookedAtColor;

    //float lookTimer = 0;
    void Start()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        defaultMatColor = meshRenderer.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        if(beingLookedAt)
        {
            lookTimer += Time.deltaTime;
            if(lookTimer > eventManager.lookDelay)
            {
                OnLookedAt();
            }
        }
        meshRenderer.material.color = Color.Lerp(defaultMatColor,lookedAtColor,Mathf.Clamp01(lookTimer/eventManager.lookDelay));
    }
    public  void OnLookedAt()
    {
        //gameObject.GetComponent<MeshRenderer>().material = mat;
        eventManager.alt = this.alt;
        Debug.Log(this.alt + " is the current alt");

    }

    public void OnStopLooking()
    {
        lookTimer = 0;
        beingLookedAt = false;
        Debug.Log(this.alt+" stopped being looked at");
    }

    public void OnStartLooking()
    {
        beingLookedAt = true;
        Debug.Log(this.alt + " started being looked at");

    }
}
